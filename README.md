# Game of Life

This is a playground project in which we 
 implement [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
 in [Angular](https://angular.io/) 
 using [ATDD](https://en.wikipedia.org/wiki/Acceptance_test%E2%80%93driven_development)
 and [Jest](https://jestjs.io/).
